/**
	exCommons - copy files to individual Wikis before deleting them on Commons
	(c) 2014 Magnus Manske
	Released under GPL v2+
*/

$(document).ready ( function () {

	var exCommons = {
	
		sites : {} ,
		no_local_upload : [ 'es.wikipedia.org' ] ,
		auto_check : [ 'en.wikipedia.org' ] ,
		empty : true ,
		oauth : 'https://tools.wmflabs.org/magnustools/oauth_uploader.php?callback=?' ,
		authorized : false ,

		init : function () {
			if ( wgAction != 'delete' ) return ; // not deleting
			if ( wgNamespaceNumber != 6 ) return ; // not a file
			var h = "<div id='ex_commons'><div id='ex_commons_oauth' style='float:right'><i>Checking OAuth...</i></div><div id='ex_commons_main'><i>Finding global usage of this file...</i></div></div>" ;
			$('#mw-content-text').prepend ( h ) ;
			this.checkOAuth() ;
			this.checkGlobalUsage() ;

			// 	$form .= "To use the automatic transfer function, you need to authorize <a href='/magnustools/oauth_uploader.php?action=authorize' target='_blank'>OAuth uploader</a> to perform uploads under your Commons user name.<br/>See also <a href='http://blog.magnusmanske.de/?p=183'>this blog entry</a>." ;
		} ,
	
		checkGlobalUsage : function ( gucontinue ) {
			var self = this ;
			var params = {
				action:'query',
				prop:'globalusage',
				gulimit:500,
				gufilterlocal:1,
				guprop:'namespace',
				format:'json' ,
				titles:wgPageName
			} ;
			if ( typeof gucontinue != 'undefined' ) params.gucontinue = gucontinue ;
			$.post ( '/w/api.php' , params , function ( d ) {
				$.each ( ((d.query||{}).pages||{}) , function ( file_page_id , v ) {
					$.each ( (v.globalusage||[]) , function ( k2 , v2 ) {
						self.empty = false ;
						if ( v2.ns == 0 ) {
							self.sites[v2.wiki] = true ; // NS 0
						} else {
							if ( typeof self.sites[v2.wiki] == 'undefined' ) self.sites[v2.wiki] = false ; // Other namespace
						}
					} ) ;
				} ) ;
				
				if ( typeof d['query-continue'] != 'undefined' ) {
					self.checkGlobalUsage ( d['query-continue'].globalusage.gucontinue ) ;
					return ;
				}
				
				self.showResults() ;
			} , 'json' ) ;
		} ,
		
		showResults : function () {
			var self = this ;
			if ( self.empty ) {
				$('#ex_commons_main').html ( "This file is not used in any other project." ) ;
				return ;
			}
			
			var h = "<h3>This file is used on other Wikimedia projects</h3>" ;
			h += "<div style='-moz-column-count:2;-webkit-column-count:2;column-count:2;-moz-column-gap: 1em;-webkit-column-gap: 1em;column-gap: 1em;'>" ;
			$.each ( self.sites , function ( site , has_ns0 ) {
				var text ;
				if ( has_ns0 ) text = "<b title='The site uses this file in an article'>" + site + "</b>" ;
				else text = "<span title='Does not use this file in an article'>" + site + "</span>" ;

				h += "<div site='" + site + "' class='ex_commons_site'>" ;
				if ( -1 != $.inArray ( site , self.no_local_upload ) ) {
					h += "<input type='checkbox' disabled />&nbsp;<s>" + text + "</s> <i>no local upload available</i>" ;
				} else {
					h += "<label><input type='checkbox' class='ex_commons_cb' site='" + site + "' " ;
					if ( has_ns0 && -1 != $.inArray ( site , self.auto_check ) ) h += " checked " ;
					h += " />&nbsp;" ;
					h += text ;
					h += "</label>" ;
				}
				h += " <span class='ex_commons_result' site='"+site+"'></span>" ;
				h += "</div>" ;
			} ) ;
			h += "</div>" ;
			h += "<input type='button' id='ex_commons_run' value='Copy file to selected wikis' />" ;
			
			// Get file description
			$.get ( '/w/api.php' , {
				action:'parse',
				page:wgPageName,
				format:'json',
				prop:'wikitext'
			} , function ( d ) {
				self.desc = d.parse.wikitext['*']||'' ;
				self.desc = self.desc.replace ( /\{\{delete.+?\}\}/i , '' ) ;
				self.desc = "{{From Commons}}\n\n" + self.desc ;
				
				$('#ex_commons_main').html ( h ) ;
				$('#ex_commons_run').click ( self.run ) ;
			} , 'json' ) ;
		} ,
		
		run : function () {
			var self = exCommons ;
			if ( !self.authorized ) {
				alert ( "OAuth is required for this to work. Please click on the link on the right and authorize, then reload this page." ) ;
				return ;
			}
			var targets = [] ;
			$('#ex_commons_main input:checked').each ( function () {
				targets.push ( $(this).attr('site') ) ;
			} ) ;
			
			var running = targets.length ;
			if ( running == 0 ) {
				alert ( "No wikis selected!" ) ;
				return ;
			}
			
			$.get ( '/w/api.php' , {
				action:'query',
				titles:wgPageName,
				prop:'imageinfo',
				format:'json',
				iiprop:'url'
			} , function ( d0 ) {
				var url ;
				$.each ( (d0.query.pages||{}) , function ( pageid , v ) {
					url = v.imageinfo[0].url ;
				} ) ;
				if ( typeof url == 'undefined' ) {
					alert ( "Can not find URL for this file; deleted already?" ) ;
					return ;
				}

				$.each ( targets , function ( dummy , site ) {
					$.getJSON ( self.oauth , {
						action:'upload',
						site:site,
						url:url,
						newfile:wgTitle,
						desc:self.desc,
						ignorewarnings:1,
						comment:'File is about to be deleted on Commons, evacuating to individual wikis',
						botmode:1
					} , function ( d ) {
						var o = $('span.ex_commons_result[site="'+site+'"]') ;
						
						console.log ( d ) ;
						if ( d.error == 'OK' ) {
							o.html ( "<span style='color:green'>OK</span>" ) ;
						} else {
							o.html ( "<span style='color:red'>"+d.res.error.info+"</span>" ) ;
						}
					
						running-- ;
						if ( running == 0 ) self.finish() ;
//						console.log ( d ) ;
					} ) ;
				} ) ;
			} , 'json' ) ;
			
		} ,
		
		finish : function () {
			// TODO
		} ,
		
		checkOAuth : function () {
			var self = this ;
			$.getJSON ( self.oauth , {
				action:'checkauth',
				botmode:1
			} , function ( d ) {
				if ( d.error == 'OK' ) {
					$('#ex_commons_oauth').html ( "OAuth OK!" ) ;
					self.authorized = true ;
				} else {
					$('#ex_commons_oauth').html ( "<a target='_blank' href='https://tools.wmflabs.org/magnustools/oauth_uploader.php?action=authorize'><b>Log into OAuth!</b></a>" ) ;
				}
			} ) ;
		} ,

		fin : ''
	} ;


	exCommons.init() ;
} ) ;